﻿using Hardware;
using System;
using System.Linq;

namespace PiranStartSetup
{
    class Program
    {
        static void Main(string[] args)
        {
            const int localPort = 39792;
            const int remotePort = 31517;

            var api = new Api(localPort, remotePort);
            const int echoCount = 3;
            const int delayMs = 1000;

            Console.WriteLine("Ищу доступные устройства...");
            string broadcastRemote = "192.168.0.255";
            var availableControllers = api.GetAvailable(broadcastRemote, echoCount, delayMs);
            foreach(var item in availableControllers)
            {
                Console.WriteLine("{0}\t{1}", item.Mac, item.Ip);
            }

            if (availableControllers.Count() != 0)
            {
                const string newIp = "192.168.0.105";
                const string newMask = "255.255.255.0";
                const string newGateway = "192.168.0.1";
                Console.WriteLine("Пробую изменить сетевые параметры у первого по MAC-адрес...");
                Console.WriteLine("Новые параметры: {0} {1} {2}", newIp, newMask, newGateway);
                HardwareInfo info = availableControllers.ElementAt(0);
                api.SetNetParameters(info.Ip, info.Mac, newIp, newMask, newGateway);
            }

            Console.WriteLine("Press any key...");
            Console.ReadKey(false);
        }
    }
}
