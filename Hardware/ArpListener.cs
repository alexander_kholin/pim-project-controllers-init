﻿using PcapDotNet.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Hardware
{
    internal class ArpListener
    {
        private List<CancellationTokenSource> _cancelListenTokenSources = new List<CancellationTokenSource>();
        private List<BufferItem> _buffer;
        const int _readTimeout = 100;

        public ArpListener()
        {
            _buffer = new List<BufferItem>();
        }

        public void Start()
        {
            IList<LivePacketDevice> allDevices = LivePacketDevice.AllLocalMachine;

            foreach (var device in allDevices)
            {
                var cancellationSource = new CancellationTokenSource();
                _cancelListenTokenSources.Add(cancellationSource);
                CancellationToken token = cancellationSource.Token;
                Task.Run(() =>
                {
                    using (PacketCommunicator communicator = device.Open(65536, PacketDeviceOpenAttributes.Promiscuous, _readTimeout))
                    {
                        communicator.SetFilter("arp");
                        while (true)
                        {
                            communicator.ReceivePackets(0, (packet) =>
                            {
                                _buffer.Add(new BufferItem
                                {
                                    Ip = string.Join(".", packet.Ethernet.Arp.SenderProtocolAddress),
                                    Mac = BitConverter.ToString(packet.Ethernet.Arp.SenderHardwareAddress.ToArray()).Replace("-", ":")
                                });
                            });

                            if (token.IsCancellationRequested)
                            {
                                break;
                            }
                        }
                    }
                }, token);
            }
        }

        public void Stop()
        {
            foreach (var cancelSource in _cancelListenTokenSources)
            {
                cancelSource.Cancel();
            }
        }

        public IEnumerable<BufferItem> Buffer
        {
            get
            {
                return _buffer;
            }
        }
    }
}
