﻿using System.Net;

namespace Hardware
{
    internal class BufferItem
    {
        public string Ip { get; set; }

        public string Mac { get; set; }

        public string Message { get; set; }
    }
}
