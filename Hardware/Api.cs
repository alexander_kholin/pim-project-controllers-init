﻿using IPHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Hardware
{
    public class Api
    {
        private readonly int _localPort;
        private readonly int _remotePort;
        private string VENDOR_CODE = "74:69:69";

        public Api(int localPort, int remotePort)
        {
            this._localPort = localPort;
            this._remotePort = remotePort;
        }

        public IEnumerable<HardwareInfo> GetAvailable(string broadcastIp, int echoCount, int delayMs)
        {
            UdpSender sender = new UdpSender();
            UdpListener listener = new UdpListener(_localPort);
            const string getNetConfigMessage = "get_net_conf\r\n";
            listener.Start();
            for (int echoNumber = 0; echoNumber < echoCount; echoNumber++)
            {
                sender.Send(broadcastIp, _remotePort, getNetConfigMessage);
                Thread.Sleep(delayMs);
            }
            listener.Stop();

            IEnumerable<HardwareInfo> infoWithoutMac = listener
                .Buffer
                .ToList()
                .Where(item => NetConfigParser.IsConfig(item.Message))
                .Select(item => new HardwareInfo
                {
                    Ip = item.Ip,
                    Gateway = NetConfigParser.ParseGateway(item.Message),
                    Mask = NetConfigParser.ParseMask(item.Message),
                    Mac = string.Empty
                })
                .Distinct(new HardwareInfoComparer());

            var arpSender = new ArpSender();
            var arpListener = new ArpListener();
            arpListener.Start();
            for (int echoNumber = 0; echoNumber < echoCount; echoNumber++)
            {
                foreach (var info in infoWithoutMac)
                {
                    arpSender.Send(info.Ip);
                }
                Thread.Sleep(delayMs);
            }
            arpListener.Stop();

            IEnumerable<HardwareInfo> ipMacPairs = arpListener
                .Buffer
                .ToList()
                .Select(item => new HardwareInfo
                {
                    Ip = item.Ip,
                    Mac = item.Mac
                })
                .Where(item => item.Mac.StartsWith(VENDOR_CODE))
                .Distinct(new HardwareInfoComparer());

            return infoWithoutMac
                .Join(
                    ipMacPairs,
                    (info) => info.Ip,
                    (info) => info.Ip,
                    (withoutMac, ipMac) => new HardwareInfo
                    {
                        Gateway = withoutMac.Gateway,
                        Ip = withoutMac.Ip,
                        Mac = ipMac.Mac,
                        Mask = withoutMac.Mask
                    }
                );
        }

        public Task<IEnumerable<HardwareInfo>> GetAvailableAsync(string broadcastIp, int echoCount, int delayMs)
        {
            return Task.Run(() =>
            {
                return GetAvailable(broadcastIp, echoCount, delayMs);
            });
        }

        public IEnumerable<HardwareInfo> GetAvailable(string startIp, string endIp, int circleCount, int delayMs)
        {
            var helper = new Helper();
            UdpSender sender = new UdpSender();
            UdpListener listener = new UdpListener(_localPort);
            const string getNetConfigMessage = "get_net_conf\r\n";
            uint startAddress = helper.IpToUint(startIp);
            uint endAddress = helper.IpToUint(endIp);

            listener.Start();
            for (int circleNumber = 0; circleNumber < circleCount; circleNumber++)
            {
                uint address = startAddress;
                while (address <= endAddress)
                {
                    sender.Send(helper.UintToIp(address), _remotePort, getNetConfigMessage);
                    address++;
                }

                Thread.Sleep(delayMs);
            }
            listener.Stop();

            IEnumerable<HardwareInfo> infoWithoutMac = listener
                 .Buffer
                 .ToList()
                 .Where(item => NetConfigParser.IsConfig(item.Message))
                 .Select(item => new HardwareInfo
                 {
                     Ip = item.Ip,
                     Gateway = NetConfigParser.ParseGateway(item.Message),
                     Mask = NetConfigParser.ParseMask(item.Message),
                     Mac = string.Empty
                 })
                 .Distinct(new HardwareInfoComparer());

            var arpSender = new ArpSender();
            var arpListener = new ArpListener();
            arpListener.Start();
            for (int echoNumber = 0; echoNumber < circleCount; echoNumber++)
            {
                foreach (var info in infoWithoutMac)
                {
                    arpSender.Send(info.Ip);
                }
                Thread.Sleep(delayMs);
            }
            arpListener.Stop();

            IEnumerable<HardwareInfo> ipMacPairs = arpListener
                .Buffer
                .ToList()
                .Select(item => new HardwareInfo
                {
                    Ip = item.Ip,
                    Mac = item.Mac
                })
                .Where(item => item.Mac.StartsWith(VENDOR_CODE))
                .Distinct(new HardwareInfoComparer());

            return infoWithoutMac
                .Join(
                    ipMacPairs,
                    (info) => info.Ip,
                    (info) => info.Ip,
                    (withoutMac, ipMac) => new HardwareInfo
                    {
                        Gateway = withoutMac.Gateway,
                        Ip = withoutMac.Ip,
                        Mac = ipMac.Mac,
                        Mask = withoutMac.Mask
                    }
                );
        }

        public Task<IEnumerable<HardwareInfo>> GetAvailableAsync(string startIp, string endIp, int circleCount, int delayMs)
        {
            return Task.Run(() =>
            {
                return GetAvailable(startIp, endIp, circleCount, delayMs);
            });
        }

        public void SetNetParameters(string ip, string mac, string newIp, string newMask, string newGateway)
        {
            UdpSender sender = new UdpSender();
            string setNetParamsMessage = string.Format("set_net_conf&ip={0}&m={1}&gw={2}\r\n", newIp, newMask, newGateway);
            sender.Send(ip, mac, _remotePort, setNetParamsMessage);
        }

        public Task SetNetParametersAsync(string ip, string mac, string newIp, string newMask, string newGateway)
        {
            return Task.Run(() =>
            {
                SetNetParameters(ip, mac, newIp, newMask, newGateway);
            });
        }
    }
}
