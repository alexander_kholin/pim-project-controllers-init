﻿using PcapDotNet.Base;
using PcapDotNet.Core;
using PcapDotNet.Packets;
using PcapDotNet.Packets.Arp;
using PcapDotNet.Packets.Ethernet;
using System;
using System.Collections.Generic;
using System.Net;

namespace Hardware
{
    internal class ArpSender
    {
        public void Send(string ip)
        {
            IList<LivePacketDevice> allDevices = LivePacketDevice.AllLocalMachine;
            foreach (var device in allDevices)
            {
                using (PacketCommunicator communicator = device.Open(65536, PacketDeviceOpenAttributes.Promiscuous, 0))
                {
                    EthernetLayer ethernetLayer = new EthernetLayer
                    {
                        Destination = new MacAddress("FF:FF:FF:FF:FF:FF"),
                        EtherType = EthernetType.None
                    };

                    ArpLayer arpLayer = new ArpLayer
                    {
                        ProtocolType = EthernetType.IpV4,
                        Operation = ArpOperation.Request,
                        SenderHardwareAddress = new byte[] { 0, 0, 0, 0, 0, 0 }.AsReadOnly(), // any available mac address
                        SenderProtocolAddress = new byte[] { 0, 0, 0, 0 }.AsReadOnly(), // any available ip address
                        TargetHardwareAddress = new byte[] { 0, 0, 0, 0, 0, 0 }.AsReadOnly(),
                        TargetProtocolAddress = IPAddress.Parse(ip).GetAddressBytes().AsReadOnly()
                    };

                    PacketBuilder builder = new PacketBuilder(ethernetLayer, arpLayer);
                    communicator.SendPacket(builder.Build(DateTime.Now));
                }
            }
        }
    }
}
