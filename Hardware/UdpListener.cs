﻿using PcapDotNet.Core;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Hardware
{
    internal class UdpListener
    {
        private List<CancellationTokenSource> _cancelListenTokenSources = new List<CancellationTokenSource>();
        private List<BufferItem> _buffer;
        const int _readTimeout = 100;
        readonly int _port;

        public UdpListener(int port)
        {
            _buffer = new List<BufferItem>();
            _port = port;
        }

        public void Start()
        {
            IList<LivePacketDevice> allDevices = LivePacketDevice.AllLocalMachine;

            foreach(var device in allDevices)
            {
                var cancellationSource = new CancellationTokenSource();
                _cancelListenTokenSources.Add(cancellationSource);
                CancellationToken token = cancellationSource.Token;
                Task.Run(() =>
                {
                    using (PacketCommunicator communicator = device.Open(65536, PacketDeviceOpenAttributes.Promiscuous, _readTimeout))
                    {
                        communicator.SetFilter(string.Format("udp port {0}", _port));
                        while (true)
                        {
                            communicator.ReceivePackets(0, (packet) =>
                            {
                                _buffer.Add(new BufferItem
                                {
                                    Ip = packet.Ethernet.IpV4.Source.ToString(),
                                    Mac = packet.Ethernet.Source.ToString(),
                                    Message = packet.Ethernet.IpV4.Udp.Payload.Decode(Encoding.ASCII).Trim('\0')
                                });
                            });

                            if (token.IsCancellationRequested)
                            {
                                break;
                            }
                        }
                    }
                }, token);
            }
        }

        public void Stop()
        {
            foreach(var cancelSource in _cancelListenTokenSources)
            {
                cancelSource.Cancel();
            }
        }

        public IEnumerable<BufferItem> Buffer
        {
            get
            {
                return _buffer;
            }
        }
    }
}
