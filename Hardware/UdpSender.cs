﻿using PcapDotNet.Core;
using PcapDotNet.Packets;
using PcapDotNet.Packets.Ethernet;
using PcapDotNet.Packets.IpV4;
using PcapDotNet.Packets.Transport;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Hardware
{
    internal class UdpSender
    {
        public void Send(string ip, int port, string dgram)
        {
            UdpClient sender = new UdpClient();
            byte[] dgramBytes = Encoding.ASCII.GetBytes(dgram);
            sender.Send(dgramBytes, dgramBytes.Length, new IPEndPoint(
                    IPAddress.Parse(ip),
                    port
                ));
        }

        public void Send(string ip, string mac, int port, string dgram)
        {
            IList<LivePacketDevice> allDevices = LivePacketDevice.AllLocalMachine;
            foreach(var device in allDevices)
            {
                using (PacketCommunicator communicator = device.Open(65536, PacketDeviceOpenAttributes.Promiscuous, 0))
                {
                    EthernetLayer ethernetLayer = new EthernetLayer
                    {
                        Source = new MacAddress(),
                        Destination = new MacAddress(mac),
                        EtherType = EthernetType.None,
                    };

                    IpV4Layer ipV4Layer = new IpV4Layer
                    {
                        Source = new IpV4Address(),
                        CurrentDestination = new IpV4Address(ip),
                        Fragmentation = IpV4Fragmentation.None,
                        HeaderChecksum = null,
                        Options = IpV4Options.None,
                        Protocol = null,
                        Ttl = 64,
                        TypeOfService = 0
                    };

                    UdpLayer udpLayer = new UdpLayer
                    {
                        DestinationPort = (ushort)port,
                        Checksum = null,
                        CalculateChecksumValue = true
                    };

                    PayloadLayer payloadLayer = new PayloadLayer
                    {
                        Data = new Datagram(Encoding.ASCII.GetBytes(dgram))
                    };

                    PacketBuilder builder = new PacketBuilder(ethernetLayer, ipV4Layer, udpLayer, payloadLayer);

                    communicator.SendPacket(builder.Build(DateTime.Now));
                }
            }
        }
    }
}
