﻿using System.Collections.Generic;

namespace Hardware
{
    internal class HardwareInfoComparer : IEqualityComparer<HardwareInfo>
    {
        public bool Equals(HardwareInfo x, HardwareInfo y)
        {
            return x.Ip == y.Ip && x.Mac == y.Mac;
        }

        public int GetHashCode(HardwareInfo obj)
        {
            return obj.Mac.GetHashCode();
        }
    }
}
