﻿using System.Text.RegularExpressions;

namespace Hardware
{
    internal static class NetConfigParser
    {
        public static string ParseMask(string netConfig)
        {
            string pattern = "\"msk\"\\s*:\\s*\"([0-9\\.]*)\"";
            return new Regex(pattern).Match(netConfig).Groups[1].Value;
        }

        public static string ParseGateway(string netConfig)
        {
            string pattern = "\"gw\"\\s*:\\s*\"([0-9\\.]*)\"";
            return new Regex(pattern).Match(netConfig).Groups[1].Value;
        }

        public static bool IsConfig(string value)
        {
            return new Regex("{.*\n\"ip\".*\n\"gw\".*\n\"msk\".*\n}").IsMatch(value);
        }
    }
}
