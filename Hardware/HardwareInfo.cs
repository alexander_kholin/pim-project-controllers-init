﻿namespace Hardware
{
    public class HardwareInfo
    {
        public string Ip { get; set; }

        public string Mac { get; set; }

        public string Mask { get; set; }

        public string Gateway { get; set; }
    }
}
