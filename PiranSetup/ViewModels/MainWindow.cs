﻿using Hardware;
using IPHelper;
using Microsoft.Win32;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace PiranSetup.ViewModels
{
    public class MainWindow : INotifyPropertyChanged
    {
        const int localPort = 39792;
        const int remotePort = 31517;

        private Controller _selectedController = null;
        private bool _isBroadcastSearch = true;
        // Broadcast search properties
        private string _broadcastAddressValue = "192.168.0.255";
        private string _broadcastAddressValueError = string.Empty;

        // Bruteforce search properties
        private string _startSearchAddressValue = "192.168.0.1";
        private string _startSearchAddressValueError = string.Empty;
        private string _endSearchAddressValue = "192.168.0.254";
        private string _endSearchAddressValueError = string.Empty;

        // loading
        private bool _displayLoading = false;

        // distribution
        private bool _isAutoDistribution = true;
        private string _distributionAddressValue = "192.168.0.1";
        private string _distributionMaskValue = "255.255.255.0";
        private string _distributionGatewayValue = "192.168.0.1";
        private string _distributionAddressValueError = string.Empty;
        private string _distributionMaskError = string.Empty;
        private string _distributionGatewayError = string.Empty;
        private string _distributionButtonError = string.Empty;
        private bool _writeReportAfterDistribution = true;

        private RelayCommand _searchCommand;
        private RelayCommand _distributeCommand;

        public ObservableCollection<Controller> Controllers { get; set; } = new ObservableCollection<Controller>();

        public Controller SelectedController
        {
            get { return _selectedController; }
            set
            {
                _selectedController = value;
                OnPropertyChanged();
            }
        }

        public bool IsBroadcastSearch
        {
            get { return _isBroadcastSearch; }
            set
            {
                _isBroadcastSearch = value;
                OnPropertyChanged();
                OnPropertyChanged("InverseIsBroadcastSearch");
                OnPropertyChanged("SearchType");
            }
        }

        public bool InverseIsBroadcastSearch
        {
            get { return !_isBroadcastSearch; }
        }

        public string BroadcastAddressValue
        {
            get { return _broadcastAddressValue; }
            set
            {
                _broadcastAddressValue = value;
                OnPropertyChanged();
            }
        }

        public string BroadcastAddressValueError
        {
            get { return _broadcastAddressValueError; }
            set
            {
                _broadcastAddressValueError = value;
                OnPropertyChanged();
            }
        }

        public string StartSearchAddressValue
        {
            get { return _startSearchAddressValue; }
            set
            {
                _startSearchAddressValue = value;
                OnPropertyChanged();
            }
        }

        public string StartSearchAddressValueError
        {
            get { return _startSearchAddressValueError; }
            set
            {
                _startSearchAddressValueError = value;
                OnPropertyChanged();
            }
        }

        public string EndSearchAddressValue
        {
            get { return _endSearchAddressValue; }
            set
            {
                _endSearchAddressValue = value;
                OnPropertyChanged();
            }
        }

        public string EndSearchAddressValueError
        {
            get { return _endSearchAddressValueError; }
            set
            {
                _endSearchAddressValueError = value;
                OnPropertyChanged();
            }
        }

        public string SearchType
        {
            get
            {
                if (_isBroadcastSearch)
                    return "Поиск по broadcast";
                else
                    return "Поиск перебором";
            }
        }

        public string DistributionAddressValueError
        {
            get { return _distributionAddressValueError; }
            set
            {
                _distributionAddressValueError = value;
                OnPropertyChanged();
            }
        }

        public string DistributionButtonError
        {
            get { return _distributionButtonError; }
            set
            {
                _distributionButtonError = value;
                OnPropertyChanged();
            }
        }

        public string DistributionCommentText
        {
            get
            {
                if (_isAutoDistribution)
                    return "IP адреса будут заданы всем контроллерам из найденного списка, начиная с указанного.";
                else
                    return "IP адрес будет задан указанному контроллеру.";
            }
        }

        public RelayCommand SearchCommand
        {
            get
            {
                return _searchCommand ??
                    (_searchCommand = new RelayCommand((obj) =>
                    {
                        var validator = new Validator();

                        if (_isBroadcastSearch)
                        {
                            bool isValidIp = validator.IsValidIp(_broadcastAddressValue);
                            BroadcastAddressValueError = isValidIp ? string.Empty : "Неверное значение";
                            
                            if(isValidIp)
                                SearchControllers();
                        }
                        else
                        {
                            bool isStartValid = validator.IsValidIp(_startSearchAddressValue);
                            bool isEndValid = validator.IsValidIp(_endSearchAddressValue);

                            StartSearchAddressValueError = isStartValid ? string.Empty : "Неверное значение";
                            EndSearchAddressValueError = isEndValid ? string.Empty : "Неверное значение";
                            
                            if (isStartValid && isEndValid)
                            {
                                var helper = new Helper();
                                int comparingValue = helper.Compare(_startSearchAddressValue, _endSearchAddressValue);
                                if (comparingValue > 0)
                                {
                                    StartSearchAddressValueError = "Не может быть больше конечного";
                                    EndSearchAddressValueError = "Не может быть меньше начального";
                                    return;
                                }
                                else
                                {
                                    StartSearchAddressValueError = string.Empty;
                                    EndSearchAddressValueError = string.Empty;
                                }

                                SearchControllers();
                            }
                        }
                    }));
            }
        }

        public RelayCommand DistributeCommand
        {
            get
            {
                return _distributeCommand ??
                    (_distributeCommand = new RelayCommand((obj) =>
                    {
                        var validator = new Validator();
                        bool isValidIp = validator.IsValidIp(_distributionAddressValue);
                        bool isValidGateway = validator.IsValidIp(_distributionGatewayValue);
                        bool isValidMask = validator.IsValidSubnetMask(_distributionMaskValue);

                        DistributionAddressValueError = isValidIp ? string.Empty : "Неверное значение";
                        DistributionGatewayError = isValidGateway ? string.Empty : "Неверное значение";
                        DistributionMaskError = isValidMask ? string.Empty : "Неверное значение";

                        bool isNeedSelectController = !_isAutoDistribution && _selectedController == null;
                        DistributionButtonError = isNeedSelectController ? "Выберите контроллер" : string.Empty;

                        bool isValid = isValidIp && isValidGateway && isValidMask && !isNeedSelectController;
                        if (isValid)
                            Distribute();
                    }));
            }
        }

        public bool DisplayLoading
        {
            get { return _displayLoading; }
            set
            {
                _displayLoading = value;
                OnPropertyChanged();
            }
        }

        public string ControllersCount
        {
            get { return Controllers.Count.ToString(); }
        }

        public bool IsautoDistribution
        {
            get { return _isAutoDistribution; }
            set
            {
                _isAutoDistribution = value;
                OnPropertyChanged();
                OnPropertyChanged("DistributionType");
                OnPropertyChanged("DistributionAddressType");
                OnPropertyChanged("DistributionCommentText");
            }
        }

        public string DistributionType
        {
            get
            {
                if (_isAutoDistribution)
                    return "Автоматически";
                else
                    return "Вручную";
            }
        }

        public string DistributionAddressType
        {
            get
            {
                if (_isAutoDistribution)
                    return "Начальный IP адрес";
                else
                    return "IP адрес";
            }
        }

        public string DistributionAddressValue
        {
            get { return _distributionAddressValue; }
            set
            {
                _distributionAddressValue = value;
                OnPropertyChanged();
            }
        }

        public string DistributionMaskValue
        {
            get { return _distributionMaskValue; }
            set
            {
                _distributionMaskValue = value;
                OnPropertyChanged();
            }
        }

        public string DistributionGatewayValue
        {
            get { return _distributionGatewayValue; }
            set
            {
                _distributionGatewayValue = value;
                OnPropertyChanged();
            }
        }

        public string DistributionMaskError
        {
            get { return _distributionMaskError; }
            set
            {
                _distributionMaskError = value;
                OnPropertyChanged();
            }
        }

        public string DistributionGatewayError
        {
            get { return _distributionGatewayError; }
            set
            {
                _distributionGatewayError = value;
                OnPropertyChanged();
            }
        }

        public bool IsDistributionVisible
        {
            get { return Controllers.Count > 0; }
        }

        public bool WriteReportAfterDistribution
        {
            get { return _writeReportAfterDistribution; }
            set
            {
                _writeReportAfterDistribution = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        private async void SearchControllers()
        {
            DisplayLoading = true;
            Api api = new Api(localPort, remotePort);
            if (_isBroadcastSearch)
            {
                IEnumerable<HardwareInfo> controllers = await api.GetAvailableAsync(_broadcastAddressValue, 3, 1000);
                Controllers = new ObservableCollection<Controller>(
                    controllers.Select(item => new Controller
                    {
                        Ip = item.Ip,
                        Mac = item.Mac,
                        Mask = item.Mask,
                        Gateway = item.Gateway
                    })
                    .OrderBy(item => item.Ip)
                );
            }
            else
            {
                IEnumerable<HardwareInfo> controllers = await api.GetAvailableAsync(_startSearchAddressValue, _endSearchAddressValue, 3, 1000);
                Controllers = new ObservableCollection<Controller>(
                    controllers.Select(item => new Controller
                    {
                        Ip = item.Ip,
                        Mac = item.Mac,
                        Mask = item.Mask,
                        Gateway = item.Gateway
                    })
                    .OrderBy(item => item.Ip)
                );
            }

            OnPropertyChanged("Controllers");
            OnPropertyChanged("ControllersCount");
            OnPropertyChanged("IsDistributionVisible");
            DisplayLoading = false;
        }

        private async void Distribute()
        {
            string newMask = _distributionMaskValue;
            string newGateway = _distributionGatewayValue;

            DisplayLoading = true;
            var api = new Api(localPort, remotePort);
            if (_isAutoDistribution)
            {
                var helper = new Helper();
                string nextIp = _distributionAddressValue;
                List<Controller> itemsForReport = new List<Controller>();

                foreach (var controller in Controllers)
                {
                    if (nextIp.EndsWith(".0"))
                        nextIp = helper.GetNextIpAddress(nextIp, 1);
                    if (nextIp.EndsWith(".255"))
                        nextIp = helper.GetNextIpAddress(nextIp, 2);

                    await api.SetNetParametersAsync(
                        controller.Ip,
                        controller.Mac,
                        nextIp,
                        newMask,
                        newGateway);

                    itemsForReport.Add(new Controller
                    {
                        Gateway = newGateway,
                        Ip = nextIp,
                        Mac = controller.Mac,
                        Mask = newMask
                    });

                    nextIp = helper.GetNextIpAddress(nextIp, 1);
                }

                if(_writeReportAfterDistribution)
                {
                    SaveReport(itemsForReport);
                }
            }
            else
            {
                await api.SetNetParametersAsync(
                    _selectedController.Ip,
                    _selectedController.Mac,
                    _distributionAddressValue,
                    newMask,
                    newGateway);
            }
            DisplayLoading = false;
            SearchControllers();
        }

        private async void SaveReport(IEnumerable<Controller> controllers)
        {
            using (var folderBrowserDialog = new FolderBrowserDialog())
            {
                folderBrowserDialog.Description = "Укажите папку для сохранения отчета.";
                DialogResult result = folderBrowserDialog.ShowDialog();
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(folderBrowserDialog.SelectedPath))
                {
                    string fileName = "Сетевые параметры контроллеров.xlsx";
                    string path = Path.Combine(folderBrowserDialog.SelectedPath, fileName);

                    Excell.ReportWriter reportWriter = new Excell.ReportWriter();
                    await reportWriter.WriteAsync(
                        controllers.Select(controller => new Excell.ReportItem
                        {
                            Gateway = controller.Gateway,
                            Ip = controller.Ip,
                            Mac = controller.Mac,
                            Mask = controller.Mask
                        }),
                        path);
                }
            }
        }
    }
}
