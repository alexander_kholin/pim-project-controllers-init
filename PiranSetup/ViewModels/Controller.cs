﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace PiranSetup.ViewModels
{
    public class Controller : INotifyPropertyChanged
    {
        private string _mac;
        private string _ip;
        private string _mask;
        private string _gateway;

        public string Mac
        {
            get { return _mac; }
            set
            {
                _mac = value;
                OnPropertyChanged();
            }
        }

        public string Ip
        {
            get { return _ip; }
            set
            {
                _ip = value;
                OnPropertyChanged();
            }
        }

        public string Mask
        {
            get { return _mask; }
            set
            {
                _mask = value;
                OnPropertyChanged();
            }
        }

        public string Gateway
        {
            get { return _gateway; }
            set
            {
                _gateway = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
