﻿using System.Text.RegularExpressions;

namespace PiranSetup
{
    internal class Validator
    {
        public bool IsValidIp(string ip)
        {
            return new Regex("^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$")
                .IsMatch(ip);
        }

        public bool IsValidSubnetMask(string mask)
        {
            var leadingOnes = "(255|254|252|248|240|224|192|128|0+)";
            var allOnes = @"(255\.)";
            var regex = new Regex("^((" + allOnes + "{3}" + leadingOnes + ")|" +
                                 "(" + allOnes + "{2}" + leadingOnes + @"\.0+)|" +
                                 "(" + allOnes + leadingOnes + @"(\.0+){2})|" +
                                 "(" + leadingOnes + @"(\.0+){3}))$");

            return regex.IsMatch(mask);
        }
    }
}
