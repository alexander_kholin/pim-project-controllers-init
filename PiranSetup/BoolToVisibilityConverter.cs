﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace PiranSetup
{
    public class BoolToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var _value = value as bool?;
            if (_value.Value == true)
                return Visibility.Visible;
            else
                return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var _value = value as Visibility?;
            if (_value.Value == Visibility.Visible)
                return true;
            else
                return false;
        }
    }
}
