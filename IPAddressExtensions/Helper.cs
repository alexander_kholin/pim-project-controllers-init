﻿using System;
using System.Linq;
using System.Net;

namespace IPHelper
{
    public class Helper
    {
        public string GetNextIpAddress(string ipAddress, uint increment)
        {
            byte[] addressBytes = IPAddress.Parse(ipAddress).GetAddressBytes().Reverse().ToArray();
            uint ipAsUint = BitConverter.ToUInt32(addressBytes, 0);
            var nextAddress = BitConverter.GetBytes(ipAsUint + increment);
            return String.Join(".", nextAddress.Reverse());
        }

        public int Compare(string firstIp, string secondIp)
        {
            uint firstAsUint = IpToUint(firstIp);
            uint secondAsUint = IpToUint(secondIp);

            return firstAsUint.CompareTo(secondAsUint);
        }

        public uint IpToUint(string ipAddress)
        {
            byte[] addressBytes = IPAddress.Parse(ipAddress).GetAddressBytes().Reverse().ToArray();
            uint ipAsUint = BitConverter.ToUInt32(addressBytes, 0);
            return ipAsUint;
        }

        public string UintToIp(uint ipAsUint)
        {
            byte[] valueBytes = BitConverter.GetBytes(ipAsUint);
            return new IPAddress(valueBytes.Reverse().ToArray()).ToString();
        }
    }
}
