﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Excell
{
    public class ReportWriter
    {
        public Task WriteAsync(IEnumerable<ReportItem> items, string path)
        {
            return Task.Run(() =>
            {
                Application app = new Application();
                app.Visible = false;
                app.DisplayAlerts = false;

                Workbook workbook = app.Workbooks.Add(Type.Missing);
                Worksheet worksheet = (Worksheet)workbook.ActiveSheet;

                worksheet.Cells[1, 1] = DateTime.Now.ToLongDateString();
                worksheet.Cells[2, 1] = "Сетевые параметры контроллеров";

                worksheet.Cells[3, 1] = "№ п/п";
                worksheet.Cells[3, 2] = "MAC";
                worksheet.Cells[3, 3] = "IP";
                worksheet.Cells[3, 4] = "Маска";
                worksheet.Cells[3, 5] = "Основной шлюз";

                int startRow = 4;
                int itemNumber = 0;
                foreach (var item in items)
                {
                    worksheet.Cells[startRow + itemNumber, 1] = (itemNumber + 1);
                    worksheet.Cells[startRow + itemNumber, 2] = item.Mac;
                    worksheet.Cells[startRow + itemNumber, 3] = item.Ip;
                    worksheet.Cells[startRow + itemNumber, 4] = item.Mask;
                    worksheet.Cells[startRow + itemNumber, 5] = item.Gateway;

                    itemNumber++;
                }

                workbook.SaveAs(path);
                workbook.Close();
                app.Quit();
            });
        }
    }
}
