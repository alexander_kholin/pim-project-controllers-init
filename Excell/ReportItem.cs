﻿namespace Excell
{
    public class ReportItem
    {
        public string Ip { get; set; }

        public string Mac { get; set; }

        public string Mask { get; set; }

        public string Gateway { get; set; }
    }
}
